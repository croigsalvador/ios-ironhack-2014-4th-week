//
//  UserProfileViewController.h
//  Shows
//
//  Created by Daniel García on 24/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserProfileViewController : UIViewController
@property (strong,nonatomic) NSManagedObjectContext *managedObjectContext;
@end
