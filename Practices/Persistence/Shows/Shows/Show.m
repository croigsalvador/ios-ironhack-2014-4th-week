//
//  Product.m
//  Models
//
//  Created by Daniel García on 17/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "Show.h"

@implementation Show
#pragma mark - NSCopying
- (id)copyWithZone:(NSZone *)zone{
	Show *productCopy=[[[self class] allocWithZone:zone] init];
	if(self){
        productCopy.showId=[self.showTitle copyWithZone:zone];
		productCopy.showTitle=[self.showTitle copyWithZone:zone];
		productCopy.showDescription=[self.showDescription copyWithZone:zone];
        
        productCopy.showRating=self.showRating;
	}
	return productCopy;
}

#pragma mark - NSCoding
- (void)encodeWithCoder:(NSCoder *)coder {
    if (self.showId) [coder encodeObject:self.showId forKey:@"showId"];
    if (self.showTitle) [coder encodeObject:self.showTitle forKey:@"showTitle"];
    if (self.showDescription) [coder encodeObject:self.showDescription forKey:@"showDescription"];
    NSNumber *priceNumber=CGFLOAT_IS_DOUBLE?[NSNumber numberWithDouble:self.showRating]:[NSNumber numberWithFloat:self.showRating];
    [coder encodeObject:priceNumber forKey:@"showRating"];
}
- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (self) {
        self.showId=[aDecoder decodeObjectForKey:@"showId"];
        self.showTitle=[aDecoder decodeObjectForKey:@"showTitle"];
        self.showDescription=[aDecoder decodeObjectForKey:@"showDescription"];
        NSNumber *priceNumber=[aDecoder decodeObjectForKey:@"showRating"];
        self.showRating=CGFLOAT_IS_DOUBLE?[priceNumber doubleValue]:[priceNumber floatValue];
    }
    return self;
}

#pragma mark - Comparison
- (BOOL)isEqualToShow:(Show *)show{
    if (self==show) {
        return YES;
    }else if(![self.showId isEqualToString:show.showId]){
        return NO;
    }
    return YES;
}

- (NSUInteger)hash{
    return [_showId hash];
}
@end
