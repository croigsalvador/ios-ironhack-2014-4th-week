//
//  LoginViewController.h
//  Shows
//
//  Created by Daniel García García on 19/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * const UserDidLogNotification = @"UserDidLogNotification";
@interface LoginViewController : UIViewController
- (id)initWithManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;
@end
