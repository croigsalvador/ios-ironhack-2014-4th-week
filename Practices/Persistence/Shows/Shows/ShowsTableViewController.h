//
//  MainTableViewController.h
//  Models
//
//  Created by Daniel García on 17/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserEntity;
@interface ShowsTableViewController : UITableViewController
@property (strong,nonatomic) NSManagedObjectContext *managedObjectContext;
@end
