//
//  ImageDownloader.m
//  Concurrency
//
//  Created by Daniel García on 26/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "ImageDownloader.h"

static NSString * const cacheFolderName = @"com.shows.imagedownloader.cache";
static NSString * const cacheKeysPlistFile = @"cacheKeys.plist";
@interface ImageDownloader()
@property (strong,nonatomic) dispatch_queue_t downloadQueue;
@property (strong,nonatomic) NSMutableDictionary *cacheKeys;
@end

@implementation ImageDownloader
+ (instancetype)defaultDownloader
{
    static dispatch_once_t onceToken;
    static ImageDownloader *instance;
    dispatch_once(&onceToken, ^{
        instance = [[ImageDownloader alloc]init];
    });
    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _downloadQueue = dispatch_queue_create("com.shows.queue.downloadImages", DISPATCH_QUEUE_CONCURRENT);
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(saveKeys) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    return self;
}
- (void)saveKeys{
    NSString *plistPath = [[self cacheFolder] stringByAppendingPathComponent:cacheKeysPlistFile];
    [self.cacheKeys writeToFile:plistPath atomically:YES];
}
- (void)downloadImageWithURL:(NSURL *)imageURL completion:(void(^)(UIImage *image))completion{
    UIImage *cachedImage = [self cachedImageForURL:imageURL];
    if (cachedImage) {
        NSLog(@"Cached Image");
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(cachedImage);
        });
    }else{
        dispatch_async(self.downloadQueue, ^{
            NSData *imageData=[NSData dataWithContentsOfURL:imageURL];
            UIImage *image = [UIImage imageWithData:imageData];
            [self cacheImage:image forURL:imageURL];
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(image);
            });
        });
    }
}

- (NSString *)cacheFolder{
    NSString *cacheFolder=[[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:cacheFolderName];
    if (![[NSFileManager defaultManager]fileExistsAtPath:cacheFolder]) {
        NSError *error;
        [[NSFileManager defaultManager]createDirectoryAtPath:cacheFolder withIntermediateDirectories:YES attributes:nil error:&error];
        if (error) {
            NSLog(@"%@",error);
        }
    }
    return cacheFolder;
}

#pragma mark - Cache
- (UIImage *)cachedImageForURL:(NSURL *)imageURL{
    UIImage *cachedImage;
    @synchronized(self){
        NSString *pathForCachedImage = [self.cacheKeys valueForKey:[imageURL description]];
        if (!pathForCachedImage) {
            return nil;
        }
        cachedImage = [self cachedImageFromPath:pathForCachedImage];
    }
    return cachedImage;
}

- (BOOL)cacheImage:(UIImage *)image forURL:(NSURL *)imageURL{
    @synchronized(self){
        NSLog(@"New Image Cache");
        NSData *imageData = UIImageJPEGRepresentation (image,1);
        NSString *imageFilePath = [[self cacheFolder] stringByAppendingPathComponent:[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]]];
        [imageData writeToFile:imageFilePath atomically:YES];
        [self.cacheKeys setValue:imageFilePath forKey:[imageURL description]];
    }
    return YES;
}


- (UIImage *)cachedImageFromPath:(NSString *)cachePath{
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfFile:cachePath]];
    return image;
}




- (NSMutableDictionary *)cacheKeys{
    if (!_cacheKeys) {
        _cacheKeys = [self cacheKeysFromPlist];
    }
    return _cacheKeys;
}
- (NSMutableDictionary *)cacheKeysFromPlist{
    NSString *plistPath = [[self cacheFolder] stringByAppendingPathComponent:cacheKeysPlistFile];
    NSDictionary *cacheKeys = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    return cacheKeys?[cacheKeys mutableCopy]:[NSMutableDictionary dictionary];
}
@end
