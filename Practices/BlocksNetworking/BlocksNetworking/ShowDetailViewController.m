//
//  ShowDetailViewController.m
//  BlocksNetworking
//
//  Created by Daniel García García on 21/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "ShowDetailViewController.h"
#import "Show.h"
#import "BlockButtonItem.h"

@interface ShowDetailViewController ()
@property (strong,nonatomic) Show *show;
@property (weak, nonatomic) IBOutlet UIImageView *posterImageView;
@property (weak, nonatomic) IBOutlet UILabel *showDescriptionLabel;
@property (strong,nonatomic) BlockButtonItem *likeShowButton;
@end

@implementation ShowDetailViewController

- (id)initWithShow:(Show *)show
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _show=show;
        self.title=show.showTitle;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self populateData];

    @weakify(self);
    self.likeShowButton=[[BlockButtonItem alloc] initWithTitle:@"Like" block:^{
        @strongify(self);
        [self likeShow];
    }];
    self.navigationItem.rightBarButtonItem = self.likeShowButton;
}

- (void)populateData{
    self.showDescriptionLabel.text=self.show.showDescription;
    self.posterImageView.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:self.show.posterImageURL]];
}

- (void)likeShow{
    
}

- (void)dealloc
{
    
}
@end
