//
//  Product.m
//  Models
//
//  Created by Daniel García on 17/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "Show.h"

@implementation Show
#pragma mark - Comparison
- (BOOL)isEqualToShow:(Show *)show{
    if (self==show) {
        return YES;
    }else if(![self.showId isEqualToString:show.showId]){
        return NO;
    }
    return YES;
}
@end
