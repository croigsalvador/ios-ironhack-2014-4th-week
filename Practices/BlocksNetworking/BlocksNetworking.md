# Blocks & Networking

## Exercise 1

- Implement a new detail view for TVShows 

- Add a Like button (UIBarButtonItem) to navigation toolbar

- Create a UIButtonBarButtonItem subclass and implement a block based callback API



## Exercise 2

- Present a UIAlertView with the "like" confirmation

- Transform the UIAlertView delegate protocol into a block based API

- Identify which case creates a retain cycle and avoid it


## Exercise 3

- Add libextobjc dependency

- Substitute weakSelf & strongSelf with the @weakify and @strongify macros


## Exercise 4

- Add AFNetworking 2.x dependency

- Create a new RequestManager class (Facade) to compose AFNetworking requests

- Create a ShowsProvider class (Client/Service) to compose specific Shows requests

- Request TVShows JSON using ShowsProvider and return a NSArray containing Shows Entities

