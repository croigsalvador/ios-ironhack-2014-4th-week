//
//  ReachabilityService.m
//  BlocksNetworking
//
//  Created by Daniel García on 25/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "ReachabilityService.h"
#import <AFNetworking/AFNetworkReachabilityManager.h>
NSString * const ReachabilityServiceChangeAvailable = @"ReachabilityServiceChangeAvailable";
NSString * const ReachabilityServiceChangeNotAvailable = @"ReachabilityServiceChangeNotAvailable";
@implementation ReachabilityService
- (instancetype)init
{
    self = [super init];
    if (self) {
        [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            switch (status) {
                case AFNetworkReachabilityStatusUnknown:
                case AFNetworkReachabilityStatusReachableViaWiFi:
                case AFNetworkReachabilityStatusReachableViaWWAN:
                    [[NSNotificationCenter defaultCenter]postNotificationName:ReachabilityServiceChangeAvailable object:nil];
                    break;
                case AFNetworkReachabilityStatusNotReachable:
                    [[NSNotificationCenter defaultCenter]postNotificationName:ReachabilityServiceChangeNotAvailable object:nil];
                    break;
            }
        }];
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    }
    return self;
}
@end
