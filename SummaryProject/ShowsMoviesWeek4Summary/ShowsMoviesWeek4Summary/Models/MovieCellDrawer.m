//
//  MovieCellDrawer.m
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 27/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "MovieCellDrawer.h"
#import "Movie.h"
#import "MovieTableViewCell.h"
@implementation MovieCellDrawer
- (void)registerCellNibInTableViewIfNeeded:(UITableView *)tableView{
    static BOOL movieCellNibRegistered = NO;
    if (!movieCellNibRegistered) {
        NSString *cellClass = NSStringFromClass([MovieTableViewCell class]);
        [tableView registerNib:[UINib nibWithNibName:cellClass bundle:nil] forCellReuseIdentifier:cellClass];
        movieCellNibRegistered = YES;
    }
}
- (UITableViewCell *)cellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath{
    [self registerCellNibInTableViewIfNeeded:tableView];
    return [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MovieTableViewCell class]) forIndexPath:indexPath];
}
- (void)drawCell:(MovieTableViewCell *)cell withItem:(Movie *)item{
    cell.textLabel.text = item.movieTitle;
}
@end
