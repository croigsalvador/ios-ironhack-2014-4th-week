//
//  TVShowCellDrawer.m
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 27/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "TVShowCellDrawer.h"
#import "TVShow.h"
#import "ShowTableViewCell.h"
@implementation TVShowCellDrawer
- (void)registerCellNibInTableViewIfNeeded:(UITableView *)tableView{
    static BOOL tvShowCellNibRegistered = NO;
    if (!tvShowCellNibRegistered) {
        NSString *cellClass = NSStringFromClass([ShowTableViewCell class]);
        [tableView registerNib:[UINib nibWithNibName:cellClass bundle:nil] forCellReuseIdentifier:cellClass];
        tvShowCellNibRegistered = YES;
    }
}
- (UITableViewCell *)cellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath{
    [self registerCellNibInTableViewIfNeeded:tableView];
    return [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ShowTableViewCell class]) forIndexPath:indexPath];
}
- (void)drawCell:(ShowTableViewCell *)cell withItem:(TVShow *)item{
    cell.textLabel.text = item.showTitle;
}
@end
