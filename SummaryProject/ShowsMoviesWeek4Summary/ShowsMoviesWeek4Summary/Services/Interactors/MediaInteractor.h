//
//  MediaInteractor.h
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 27/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MediaInteractor : NSObject
- (void)mediaItemsWithCompletion:(void(^)(NSArray *mediaItems))completion;
@end
