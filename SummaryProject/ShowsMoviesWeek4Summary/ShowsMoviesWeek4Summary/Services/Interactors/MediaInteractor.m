//
//  MediaInteractor.m
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 27/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "MediaInteractor.h"
#import "TVShowsProvider.h"
#import "MoviesProvider.h"

@interface MediaInteractor ()
@property (strong,nonatomic) MoviesProvider *moviesProvider;
@property (strong,nonatomic) TVShowsProvider *showsProvider;
@end
@implementation MediaInteractor
- (instancetype)init
{
    self = [super init];
    if (self) {
        _moviesProvider = [[MoviesProvider alloc] init];
        _showsProvider = [[TVShowsProvider alloc] init];
    }
    return self;
}
- (void)mediaItemsWithCompletion:(void(^)(NSArray *mediaItems))completion{
    __block NSMutableArray *mediaItems = [NSMutableArray array];
    dispatch_group_t dispatchGroup = dispatch_group_create();
    
    dispatch_group_enter(dispatchGroup);
    [self.moviesProvider moviesWithSuccessBlock:^(NSArray *movies) {
        [mediaItems addObjectsFromArray:movies];
        dispatch_group_leave(dispatchGroup);
    } errorBlock:^(NSError *error) {
        
    }];
    
    dispatch_group_enter(dispatchGroup);
    [self.showsProvider showsWithSuccessBlock:^(NSArray *shows) {
        [mediaItems addObjectsFromArray:shows];
        dispatch_group_leave(dispatchGroup);
    } errorBlock:^(NSError *error) {
        
    }];
    
    
    dispatch_group_notify(dispatchGroup, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSArray *shuffledArray = [self shuffledArray:mediaItems];
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(shuffledArray);
        });
    });
    
}
- (NSArray *)shuffledArray:(NSArray *)array{
    NSMutableArray *shuffledArray = [array mutableCopy];
    NSUInteger count = [shuffledArray count];
    for (NSUInteger i = 0; i < count; ++i) {
        NSInteger remainingCount = count - i;
        NSInteger exchangeIndex = i + arc4random_uniform(remainingCount);
        [shuffledArray exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
    }
    return shuffledArray;
}
@end
