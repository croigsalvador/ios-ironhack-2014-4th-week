//
//  BaseProvider.h
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 27/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import "RequestManager.h"

@interface BaseProvider : NSObject
@property (strong,nonatomic) RequestManager *requestManager;
@end
