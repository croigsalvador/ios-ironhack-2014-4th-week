//
//  MediaItemProtocol.h
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 27/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CellDrawerProtocol.h"

@protocol MediaItemProtocol <NSObject>
@property (copy,nonatomic,readonly) NSString *itemTitle;
@property (copy,nonatomic,readonly) NSString *itemID;
@property (strong,nonatomic,readonly) id<CellDrawerProtocol> cellDrawer;
- (UIViewController *)detailViewController;
@end
