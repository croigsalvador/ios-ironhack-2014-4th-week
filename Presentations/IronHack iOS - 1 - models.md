#Models

## IronHack iOS Bootcamp 
##![100%,inline](assets/ironhacklogo.png)
### Week 4 - Day 1


####Daniel García - Produkt
---

#What is a model?

>"…the information representation that the system operates with. Therefore manages all access to this information, from queries to updates. All operation described on the application specifications. …"
--Wikipedia (Spanish version)

---

![left](assets/MVC-Process.png)

#MVC

---

#Information repository that is application independent

---

#~~Model~~ --> Entity

---

#Basic operations

##Copy, Save, Compare, Search

---
![fill](assets/duplicate.png)
#Copy
---
#<NSCopying>

Allow us to respond to the **"copy"** selector, returning a *new object* with *identical values*

```objectivec
NSString *productName=@“iPad”;
return [productName copy];

```
---
# super <NSCopying>

```objectivec
- (id)copyWithZone:(NSZone *)zone{
	ProductEntity *productCopy=[super copyWithZone:zone];
	if(productCopy){
		// Objects
		productCopy.name=[self.name copyWithZone:zone];
		productCopy.reference=[self.reference coypWithZone:zone];

		// Scalars
		productCopy.price=self.price;
	}
	return productCopy;
}
```
---
# super ~~<NSCopying>~~

```objectivec
- (id)copyWithZone:(NSZone *)zone{
	ProductEntity *productCopy=[[[self class] allocWithZone:zone] init];
	if(productCopy){
		// Objects
		productCopy.name=[self.name copyWithZone:zone];
		productCopy.reference=[self.reference coypWithZone:zone];
		
		// Scalars
		productCopy.price=self.price;
	}
	return productCopy;
}
```

--- 

# NSZone

## Zone of memory reserved for the object instantiation. If zone is NULL, the new instance is allocated from the default zone

```objectivec
[productObject copy] == [productObject copyWithZone:nil]
```

--- 

# <NSMutableCopying>

--- 

# <NSMutableCopying>

##Exactly the same as <NSCopying> but for object with “immutable vs. mutable” distinction (NSArray, NSMutableArray)

###Classes that don’t define such a distinction should adopt <NSCopying> instead

--- 
![fill](assets/safe.png)
#Save

##Serialise an object into data that can be *archived* to disk

--- 

#<NSCoding>
```objectivec
@interface ProductEntity : NSObject <NSCoding>
@property NSString *name;
@property NSString *reference;
@property CGFloat price;
@property (getter = isAvailable) BOOL available;
@end
```
--- 
#<NSCoding> (Serialise) 
```objectivec
- (void)encodeWithCoder:(NSCoder *)encoder {
	[encoder encodeObject:self.name forKey:@"name"];
	[encoder encodeObject:self.reference forKey:@"reference"];
	[encoder encodeFloat:self.price forKey:@"price"];
	[encoder encodeBool:[self isAvailable] forKey:@"available"];
}
```

--- 
#<NSCoding> (Deserialise)
```objectivec
- (id)initWithCoder:(NSCoder *)decoder {
	self = [super init];
	if (self) {
		self.name = [decoder decodeObjectForKey:@"name"];
		self.reference = [decoder decodeObjectForKey:@"reference"];
		self.price = [decoder decodeFloatForKey:@"price"];
		self.available = [decoder decodeBoolForKey:@"available"];
	}
	return self;
}
```
--- 

#NSKeyedArchiver/NSKeyedUnarchiver

##Concrete classes of NSCoder that allow us to serialise an object and archive or viceversa

--- 
#Serialise
```objectivec
[NSKeyedArchiver archiveRootObject:product toFile:@"/path/to/archive"];
```

#Deserialise
```objectivec
ProductEntity *product=[NSKeyedUnarchiver unarchiveObjectWithFile:@"/path/to/archive"];
```
--- 

#PRACTICE

^ 
- Create Show Model
- Implement <NSCopying> 
- Implement <NSCoding>

--- 
![fill](assets/compare.png)
#Compare

##isEqual:

--- 

#Equality (isEqual:) vs. Identity (==)

## Two object are *equals* if all it's observable properties (public) are equals or equivalents, either these two abject are distinct objects with it's own identity (memory address)

--- 

##If we want an object to be comparable, by 


## Subclasses of NSObject implementing their own isEqual: method are expected to do the following:

1. Implement a new isEqualTo__ClassName__: method, which performs the meaningful value comparison.
2. Override isEqual: to make class and object identity checks, falling back on a value comparison method.
3. Override *hash*

--- 

#ProductEntity
```objectivec
@interface ProductEntity : NSObject 
@property NSString *name;
@property NSString *reference;
@property CGFloat price;
@property (getter = isAvailable) BOOL available;
@end
```

--- 

#1) isEqualTo__ ClassName __:

```objectivec
- (BOOL)isEqualToProductEntity:(ProductEntity *)productEntity{
	if(![self.name isEqualToString:productEntity.name]){
		return NO;
	}
	if(![self.reference isEqualToString:productEntity.reference]){
		return NO;
	}	
	return YES;
}
```
--- 

#2) isEqual:

```objectivec
- (BOOL)isEqual:(id)anObject{
	if(self==anObject){
		return YES;
	}
	if(![anObject isKindOfClass:[self class]]){
		return NO;
	}
	return [self isEqualToProductEntity:(ProductEntity *)anObject];
}
```
--- 
#3) hash

*hash* is a unique identifier (NSUInteger)

Two objects equals using isEqual: method, should return the same *"hash"* value


```objectivec
- (NSUInteger)hash{
	return [_reference hash];
}
```

--- 

#3) hash

hash will be accessed A LOT of times. hash implementation should be “lightning fast”.

A simple XOR over the hash values of critical properties is sufficient 99% of the time

####https://www.mikeash.com/pyblog/friday-qa-2010-06-18-implementing-equality-and-hashing.html


```objectivec
- (NSUInteger)hash
{
	return [_firstName hash] ^ [_lastName hash]; 
}
```
--- 

# Search

##(containsObject:) , NSArray , NSSet , NSDictionary, …

--- 

##**hash** implementation makes faster search and comparison of our objects inside collections

--- 

#PRACTICE

--- 

# Mantle

#![100%,inline](assets/7318467.png)

--- 

#Mantle
1. Library created by some of GitHub engineers that automatically implements **NSCoding**, **NSCopying**, **isEqual:**, and **hash** in our class.
2. Additionally implements all needed for parsing **JSON** objects turning them into our own entities.
3. It also allow us to convert our entities into **JSON** objects.

--- 

# Subclass MTLModel

## Automagically implements **NSCoding**, **NSCopying**, **isEqual:**, y **hash**.

--- 

#PRACTICE

--- 

# <MTLJSONSerializing>

## **JSON** -> Entity -> **JSON**

--- 

#PRACTICE
